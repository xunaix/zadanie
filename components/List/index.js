import React from 'react'
import styles from '../../styles/Home.module.css'
import Image from 'next/image'

const List = ({ repositories }) => {
  return (
    <>
      {repositories.map(({ node }, i) =>
        <div key={i} className={styles.card}>
          <a target="_blank" rel="noreferrer" href={node.url}>
            <h2>{node.name}</h2>
            <p>{node.description}</p>
          </a>
          <a className={styles.user} target="_blank" rel="noreferrer" href={`https://github.com/${node.owner.login}`}>
            <p>{node.owner.login}</p>
            <Image src={node.owner.avatarUrl} width={150} height={150} className={styles.avatar} alt="avatar"/></a>
        </div>)}
    </>
  )
}

export default List
