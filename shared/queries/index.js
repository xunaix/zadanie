import { gql } from '@apollo/client'

export const SEARCH = gql`
    query Search($query: String!, $after: String) {
        search(query: $query, type: REPOSITORY, first: 10, after: $after) {
            edges {
                node {
                    ... on Repository {
                        name
                        url
                        description
                        owner {
                            avatarUrl
                            login
                        }
                    }
                }
            }
            pageInfo {
                hasNextPage
                endCursor
            }
        }
        }
    
`;
